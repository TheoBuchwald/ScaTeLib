# set cdash buildname
set(BUILDNAME
   "${CMAKE_SYSTEM_NAME}-${CMAKE_HOST_SYSTEM_PROCESSOR}-${CMAKE_Fortran_COMPILER_ID}-${BLAS_TYPE}-${CMAKE_BUILD_TYPE}"
   CACHE STRING
   "Name of build on the dashboard"
   )

# set ctest own timeout
set(DART_TESTING_TIMEOUT
   "1200"
   CACHE STRING
   "Set timeout in seconds for every single test"
   )


macro(add_tensor_test _num _name _labels)
   if(ENABLE_MPI)
      add_test( ${_name} mpirun -n 4 ${PROJECT_BINARY_DIR}/src/ScaTeLib.x ${_num})
   else()
      add_test( ${_name} ${PROJECT_BINARY_DIR}/src/ScaTeLib.x ${_num})
   endif()
   if(NOT "${_labels}" STREQUAL "")
      set_tests_properties(${_name} PROPERTIES LABELS "${_labels}")
   endif()
endmacro()

#add_tensor_test(1  "tensor_struct"  "tensor;struct")
#add_tensor_test(2  "tensor_example" "tensor")
add_tensor_test(3  "reorderings"    "reorder;unit")
if(ENABLE_TENSOR_CLASS)
   add_tensor_test(4  "new_structs"    "struct")
   add_tensor_test(5  "dot"            "func;unit")
   add_tensor_test(6  "dot_master"     "func;unit")
   add_tensor_test(7  "add"            "func;unit")
   add_tensor_test(8  "add_master"     "func;unit")
   add_tensor_test(9  "hmul"           "func;unit")
   add_tensor_test(10 "hmul_master"    "func;unit")
endif(ENABLE_TENSOR_CLASS)
add_tensor_test(11  "IO"    "IO")

include(CTest)
enable_testing()
