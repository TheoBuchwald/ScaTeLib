!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Author:   Patrick Ettenhuber (pettenhuber@gmail.com)
! Date:     approximately 2015
! File:     This file contains a simple test program for a few FORTRAN 2003
!           standard features. This is mainly focused on the PGI compiler since
!           it was not able to compile the contains statement in the main
!           program.
! License : This file is subject to all the licensing conditions under which
!           ScaTeLib is distributed
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module typedefmod
   type vec
      real,pointer,contiguous :: t(:)
   end type vec
   
   type tile
      real,pointer,contiguous :: t(:) => null()
   end type tile
end module typedefmod
  
module test_module
   use,intrinsic :: iso_c_binding
   implicit none

   !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
   !Date:   approximately 2015
   !Intent Test interface to add
   interface add
     module procedure add_int,add_real
   end interface add
   !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
   !Date:   approximately 2015
   !Intent: Test interface to multiply
   interface mult
     module procedure mult_int,mult_real
   end interface mult

   abstract interface
     !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
     !Date:   approximately 2015
     !Intent: abstract interface for a real add or mult
     subroutine testroutine(a,b,c)
       real,intent(in)  :: a,b
       real,intent(out) :: c
     end subroutine
   end interface

   contains

   !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
   !Date:   approximately 2015
   !Intent: Test different kinds of pointer associations, but mainly whether a
   !        given compiler is able to compile this
   subroutine my_test
     implicit none
     type(c_ptr)  :: cptr
     real,pointer :: fptr1(:)
     real,pointer :: fptr2(:)
     real,pointer :: fptr3(:,:,:)
     real,pointer :: fptr4(:,:,:,:)
     logical      :: success
     integer      :: a,b,c
     procedure(testroutine), pointer :: funcptr => null()
    
     allocate(fptr1(8))
    
     cptr = c_loc(fptr1(1))
    
     if(c_associated(cptr))then
       call c_f_pointer(cptr,fptr2,[8])
     endif
    
     cptr = c_null_ptr
    
     if(associated(fptr2))then
       call random_number(fptr2)
       fptr2(3) = fptr2(1) + fptr2(2)
       fptr2(6) = fptr2(4) * fptr2(5)
     endif
    
     success = .true.
   
     funcptr => add_real

     call funcptr(fptr1(1),fptr1(2),fptr1(7))
    
     funcptr => mult_real
     
     call funcptr(fptr1(4),fptr1(5),fptr1(8))
    
     if(fptr1(3)/=fptr1(7).or.fptr1(6)/=fptr1(8)) success = .false.
     if(c_associated(cptr)) success = .false.
     fptr2 => null()

     !Test pointer reshape I
     call c_f_pointer(c_loc(fptr1(1)),fptr3,[2,2,2])

     do a=1,2
        do b=1,2
           do c=1,2
              if(fptr3(a,b,c) /= fptr1(a+(b-1)*2+(c-1)*4))then
                 success = .false.
              endif
           enddo
        enddo
     enddo

     !Test pointer reshape II
     !fptr4(1:2,1:1,1:2,1:2) => fptr1

     !do a=1,2
     !   do b=1,2
     !      do c=1,2
     !         if(fptr4(a,1,b,c) /= fptr1(a+(b-1)*2+(c-1)*4))then
     !            success = .false.
     !         endif
     !      enddo
     !   enddo
     !enddo

     deallocate(fptr1)

     fptr1 => null()
     fptr3 => null()
     fptr4 => null()

     print *,success
     flush(6)
   end subroutine my_test

   !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
   !Date:   approximately 2015
   !Intent: Simple real multiplication
   !Input :
   !       - a: input arg 1
   !       - b: input arg 2
   !Output :
   !       - c: c = a*b
   subroutine mult_real(a,b,c)
     real, intent(in)  :: a,b
     real, intent(out) :: c
     c = a * b
   end subroutine mult_real
   !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
   !Date:   approximately 2015
   !Intent: Simple integer multiplication
   !Input :
   !       - a: input arg 1
   !       - b: input arg 2
   !Output :
   !       - c: c = a*b
   subroutine mult_int(a,b,c)
     integer, intent(in)  :: a,b
     integer, intent(out) :: c
     c = a * b
   end subroutine mult_int

   !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
   !Date:   approximately 2015
   !Intent: Simple real addition
   !Input :
   !       - a: input arg 1
   !       - b: input arg 2
   !Output :
   !       - c: c = a+b
   subroutine add_real(a,b,c)
     real, intent(in)  :: a,b
     real, intent(out) :: c
     c = a + b
   end subroutine add_real
   !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
   !Date:   approximately 2015
   !Intent: Simple integer addition
   !Input :
   !       - a: input arg 1
   !       - b: input arg 2
   !Output :
   !       - c: c = a+b
   subroutine add_int(a,b,c)
     integer, intent(in)  :: a,b
     integer, intent(out) :: c
     c = a + b
   end subroutine add_int

end module test_module


program test
   use test_module     
   use typedefmod
   implicit none
   real,pointer :: fptr1(:)
   real,pointer :: fptr2(:)
   real,pointer,contiguous :: fptr3(:,:,:)
   logical      :: success
   integer      :: a,b,c,n1,n2
   real,pointer :: matV(:)
   type(vec)    :: vect
   type(tile),pointer   :: tt(:)
   real,pointer,contiguous :: tpm(:,:)

   call my_test
     
   n1=3
   n2=5
   allocate(tt(3))
   tpm(1:n1,1:n2) => tt(1)%t

   matV(1:5) => vect%t

   allocate(fptr1(12))
   call random_number(fptr1)

   !Test pointer reshape II                                                                                        
   fptr3(1:2,1:2,1:2) => fptr1(4:)
   success = .true.

   do a=1,2
      do b=1,2
         do c=1,2
            if(fptr3(a,b,c) /= fptr1(a+(b-1)*2+(c-1)*4+3))then
               success = .false.
            endif
         enddo
      enddo
   enddo

   deallocate(fptr1)

   fptr1 => null()
   fptr3 => null()

   print *,success

   contains

   !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
   !Date:   approximately 2015
   !Intent: Pointer association in a subroutine that is in a contains statement
   !        block. Some PGI was not able to compile this
   !Input :
   !       - finp: some array
   !       - n1:   dimension 1 of finp
   !       - n2:   dimension 1 of finp
   subroutine test_in_subroutine(finp,n1,n2)
      implicit none
      integer, intent(in) :: n1,n2
      real, target :: finp(n1,n2)
      real, pointer :: f(:)
      f(1:n1*n2) => finp
   end subroutine test_in_subroutine
  
end program test

