module tensor_parameters_module
   !Define atomic data types used in the tensor module
   integer, parameter :: tensor_dp           = 8
   integer, parameter :: tensor_sp           = 4
#ifdef VAR_INT64
   integer, parameter :: tensor_int          = 8
   integer, parameter :: tensor_log          = 8
   integer, parameter :: tensor_max_int      = 9223372036854775800
#else
   integer, parameter :: tensor_int          = 4
   integer, parameter :: tensor_log          = 4
   integer, parameter :: tensor_max_int      = 2147483640
#endif
   integer, parameter :: tensor_standard_int = 4
   integer, parameter :: tensor_standard_log = 4
   integer, parameter :: tensor_long_int     = 8
   integer, parameter :: tensor_long_log     = 8
   integer, parameter :: tensor_char_size    = 1
   integer, parameter :: tensor_char_4       = 4
#ifdef VAR_INT64
#ifdef VAR_MPI_32BIT_INT
   integer, parameter :: tensor_mpi_kind     = 4
#else
   integer, parameter :: tensor_mpi_kind     = 8
#endif
#else
   integer, parameter :: tensor_mpi_kind     = 4
#endif
   integer, parameter :: tensor_name_length  = 256

   !L2_CACHE_SIZE = 256000 ! lower estimate of cache size in bytes:
   !BS_2D = floor(sqrt(L2_CACHE_SIZE/(2*8.0E0_realk)))
   !BS_3D = floor((L2_CACHE_SIZE/(2*8.0E0_realk))**(1/3.0E0_realk))
   !BS_4D = floor((L2_CACHE_SIZE/(2*8.0E0_realk))**(1/4.0E0_realk))
   !
   !We write the explicit values to avoid internal compiler error 
   !with the gnu compiler 4.4.7 and the power function:
   integer,parameter :: BS_2D = 126
   integer,parameter :: BS_3D =  25
   integer,parameter :: BS_4D =  11

   !MPI SIGNAL, GET SLAVES, MAKE SURE THE APPLICATION HAS NO OVERLAPPING SIGNAL
   integer,parameter :: TENSOR_SLAVES_TO_SLAVE_ROUTINE_STD =  -121
   integer ::           TENSOR_SLAVES_TO_SLAVE_ROUTINE     = TENSOR_SLAVES_TO_SLAVE_ROUTINE_STD

   !MPI COMM TO USE IN TENSOR OPERATIONS, THIS IS UPDATED AT RUNTIME
   integer(tensor_mpi_kind), parameter :: tensor_comm_null = -124
   integer(tensor_mpi_kind), pointer   :: tensor_work_comm => null()

   !parameters to define the data distribution in the tensor type
   integer(tensor_standard_int), parameter :: TT_None                   = 0
   integer(tensor_standard_int), parameter :: TT_DenseTensor            = 1
   integer(tensor_standard_int), parameter :: TT_DenseReplicatedTensor  = 2
   integer(tensor_standard_int), parameter :: TT_TiledTensor            = 3
   integer(tensor_standard_int), parameter :: TT_TiledDistributedTensor = 4
   integer(tensor_standard_int), parameter :: TT_TiledReplicatedTensor  = 5
   
   !parameters for ACCESS TYPE:
   integer(tensor_standard_int),parameter :: AT_None   = 0
   integer(tensor_standard_int),parameter :: AT_Master = 1
   integer(tensor_standard_int),parameter :: AT_All    = 2
   
   !other parameters
   integer,parameter :: TENSOR_MSG_LEN = 30
   integer,parameter :: DEFAULT_TDIM   = 40
   
   integer,parameter :: lspdm_stdout  = 6
   integer,parameter :: lspdm_errout  = 0
   
   !parameter to reduce the amount of windows per array
#ifdef VAR_HAVE_MPI3
   logical,parameter :: alloc_in_dummy = .true.
#else
   logical,parameter :: alloc_in_dummy = .false.
#endif

   integer(tensor_standard_int), parameter :: tensor_max_int_standard = 2147483640
   integer(tensor_long_int), parameter     :: long1                   = 1_tensor_long_int
   
   character, parameter :: allowed_fname_chars(62) = ["a","b","c","d","e","f","g","h","i","j","k",&
        &"l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E", &
        &"F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",&
        &"0","1","2","3","4","5","6","7","8","9"]

   character(4), parameter :: tns_ext = ".tns"
end module tensor_parameters_module
