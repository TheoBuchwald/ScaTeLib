      integer(tensor_long_int) :: vector_size
      integer :: i
      if (present(mode))then
        if((arr%mode/=0 .and. arr%mode/=mode).or.arr%mode==0)then
          print *,"mode",mode,"arr%mode",arr%mode      
          call tensor_status_quit("wrong use of tensor_set_dims",6346)
        else
          arr%mode=mode
        endif
      endif
      vector_size = int(arr%mode*tensor_int,kind=tensor_long_int)
      if (associated(arr%dims))then
!$OMP CRITICAL
        tensor_counter_aux_f_mem = tensor_counter_aux_f_mem + vector_size
        tensor_counter_memory_in_use    = tensor_counter_memory_in_use  - vector_size
!$OMP END CRITICAL
        call tensor_free_mem(arr%dims)
      endif
      if(.not.associated(arr%dims))then
        call tensor_alloc_mem(arr%dims,arr%mode)
!$OMP CRITICAL
        tensor_counter_aux_a_mem = tensor_counter_aux_a_mem + vector_size
        tensor_counter_memory_in_use  = tensor_counter_memory_in_use  + vector_size
!$OMP END CRITICAL
      endif
      do i=1,arr%mode
        arr%dims(i)=dims(i)
      enddo
