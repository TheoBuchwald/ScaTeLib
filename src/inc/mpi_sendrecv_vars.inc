      integer(tensor_mpi_kind),intent(in) :: comm
      integer(tensor_mpi_kind),intent(in) :: sender, receiver
      integer(tensor_mpi_kind) :: ierr, datatype, nMPI, tag, stat(MPI_STATUS_SIZE), rank
      integer(tensor_long_int) :: n, chunk, first_el, datatype_size
      call tensor_get_rank_for_comm(comm,rank)
