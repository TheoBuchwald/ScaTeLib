
        if( simple )then
           ! load new tiles
           select type(B)
           class is (TiledDistributedTensor)
              if( .not. B%mpi%offset == A%mpi%offset)then
                 call fill_buffer_for_blas1_like(asyncB,ltA,A,B,buffB,o,.true.)
              endif
           end select
        endif
     enddo LocalTiles

     if(.not.B_dense)then
        call tensor_async_buffer_check_no_l(asyncB)
        call tensor_async_buffer_free(asyncB)
     endif

     call tensor_buffer_spec_unset(buf_)

     if( alloc_in_dummy )then

        if(.not.B_dense)then
           if(.not.locB)call BasicTensor_unlock_wins(B,all_nodes=.true.)
        endif

     endif

     !critical barrier if synchronization is not achieved by other measures
     if( tensor_always_sync .or. fs ) call tensor_mpi_barrier(tensor_work_comm)

     call tensor_stack_pop()
