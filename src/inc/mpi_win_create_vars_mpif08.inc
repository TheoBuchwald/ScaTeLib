
       type(MPI_Win), intent(inout)  :: win
       type(MPI_Comm), intent(inout) :: comm
       type(MPI_Datatype) :: datatype
       integer(tensor_mpi_kind)  :: ierr=0
       integer(tensor_mpi_kind)  :: info,s
       integer(MPI_ADDRESS_KIND) :: mpi_size,lb,b
       integer(tensor_long_int)  :: n
