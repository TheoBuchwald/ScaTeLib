bname=$(basename "$0")
bname=${bname/.bash/}"_$CI_BUILD_REF_NAME"
##########
export LM_LICENSE_FILE=/opt/pgi/license.dat
source /opt/pgi/linux86-64/15.10/pgi.sh
source /opt/pgi/linux86-64/15.10/mpi.sh
export LD_LIBRARY_PATH=/opt/pgi/15.10/share_objects/lib64:$LD_LIBRARY_PATH
export DALTON_NUM_MPI_PROCS=6
export OMP_NUM_THREADS=2
export LSDALTON_DEVELOPER=1
#
wrk=$1
#
if [ ! -d $bname ]   
then
   ./setup --fc=mpif90 --omp --mpi --type=debug --cmake-options="-DBUILDNAME=$bname" $bname
fi
#
cd $bname
#
#ctest -D Nightly --track GitLabCI -L ContinuousIntegration
ctest -D Nightly --track GitLabCI
exit $?
